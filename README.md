# Meetup Rust - Clap

A [RevealJS](https://revealjs.com/) slideshow written in [Markdown](https://daringfireball.net/projects/markdown/syntax) and converted using [Pandoc](https://pandoc.org/).

It's been made for the Grenoble Rust Meetup on the 11th of January of 2024 hosted by [La Turbine Coop](https://turbine.coop/).
