index.html: index.md
	pandoc index.md --output index.html --from markdown+emoji --to revealjs --standalone --slide-level 4

.PHONY: clean
clean:
	rm index.html
